package com.httpUtils;

import com.utils.FileUtils;

import java.io.File;
import java.io.IOException;

/**
 * 文件参数
 */
public class FileParm {

    public  FileParm(File f) {
        file = f;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    File file;
    private String name;

    private String value;

    public String getMime() {
        return"application/octet-stream";

    }

    public void setMime(String mime) {
        this.mime = mime;
    }

    public String getName() {
        return getFile().getName();
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getValue() {

        byte[] bytes = new byte[1000];
        try {
            bytes = FileUtils.toBytes(getFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bytes;
    }

    public void setValue(String value) {
        this.value = value;
    }

    private String mime;


}

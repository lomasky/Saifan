package com.httpUtils;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import java.util.List;
import java.util.Map;

/**
 * loma
 * Created by Administrator on 2015/6/24.
 */
public class Http {


    /**
     * 返回对象 带进度条 post 可选择显示进度
     *
     * @param context
     * @param api
     * @param clazz
     * @param parms
     * @param tip
     * @param listener
     */
    public static <T> void post(Context context, String api, Object parms,  @Nullable String tip,   final Class<T> clazz, final ListResponseListener<T> listener) {
        RequestManager.post(context, URLConfig.URL_ROOT + api, parms, tip, clazz, listener);
    }


    /**
     * 返回对象 带进度条 post 可选择显示进度
     *
     * @param context
     * @param api
     * @param clazz
     * @param parms
     * @param tip
     * @param listener
     */
    public static <T> void post(Fragment context, String api, Object parms,  @Nullable String tip, @Nullable final Class<T> clazz, final ListResponseListener<T> listener) {
        RequestManager.post(context, URLConfig.URL_ROOT + api, parms, tip, clazz, listener);
    }





    /**
     * 返回对象 带进度条 post 可选择显示进度
     *
     * @param context
     * @param api 接口

     * @param tip
     * @param listener
     */
    public static <T> void post(Context context, String api,   List<FileParm> fileParms,   Map<String , Object> requstParms, @Nullable String tip,   final StringResponseListener listener) {
        RequestManager.post(context, URLConfig.URL_ROOT + api,  fileParms,requstParms, tip,   listener);
    }

    /**
     * 返回对象 带进度条 post 可选择显示进度
     *
     * @param context
     * @param api
     * @param fileParms
     * @param tip
     * @param listener
     */
    public static <T> void post(Fragment  context , String api,   List<FileParm> fileParms, Map<String , Object> requstParms,   @Nullable String tip,   final StringResponseListener listener) {
        RequestManager.post(context, URLConfig.URL_ROOT + api,  fileParms,requstParms, tip,   listener);
    }

    public static void cancel(Object object) {

        RequestManager.cancelAll(object);
    }


}

package com.httpUtils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.utils.CommonUtils;
import com.utils.JsonUtil;
import com.utils.StringUtil;
import com.utils.ToastUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;

/**
 * loma
 * Created by Administrator on 2015/6/24.
 */
@SuppressLint("NewApi")
public class RequestManager {
    public static Context context;
    public static LoadingFragment dialog = null;
    private static RequestQueue requestQueue;

    private RequestManager() {
    }

    public static RequestQueue getRequestQueue() {

        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(context );
        }
        return requestQueue;
    }






    /**
     * 返回对象 带进度条
     *
     * @param url      接口
     * @param context  上下文
     * @param fileParms   post需要传的文件list
     * @param tip      进度条文字
     * @param listener 回调
     */
    public static <T> void post(final Context context, String url, List<FileParm> fileParms,@Nullable Map<String ,Object> requstParms,@Nullable final String tip, final StringResponseListener listener) {

        if (!CommonUtils.isNetWorkConnected(context)) {
            ToastUtil.showToast(context, "网络异常");

            return;
        }


        if (!StringUtil.isNullOrEmpty(tip)) {
            dialog = new LoadingFragment();
            dialog.show(((FragmentActivity) context).getSupportFragmentManager(), "Loading");
            dialog.setMsg(tip);
        }
        FileUploadRequest request = new FileUploadRequest(url, fileParms, requstParms, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {

                if (!StringUtil.isNullOrEmpty(tip)) {
                    if (dialog.getShowsDialog()) {
                        dialog.dismiss();
                    }
                }
                //不做处理，交给应用解析
                listener.onResult(s);



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onError(error);
                if (!StringUtil.isNullOrEmpty(tip)) {
                    if (dialog.getShowsDialog()) {
                        dialog.dismiss();
                    }
                }
            }
        });

        addRequest(request, context);
    }


    /**
     * 返回对象 带进度条
     *
     * @param url      接口
     * @param context  上下文
     * @param fileParms   post需要传的文件list
     * @param tip      进度条文字
     * @param listener 回调
     */
    public static <T> void post(final Fragment context, String url, List<FileParm> fileParms,@Nullable Map<String ,Object> requstParms,@Nullable final String tip, final StringResponseListener listener) {

        if (!CommonUtils.isNetWorkConnected(context.getActivity())) {
            ToastUtil.showToast(context.getActivity(), "网络异常");

            return;
        }


        if (!StringUtil.isNullOrEmpty(tip)) {
            dialog = new LoadingFragment();
            dialog.show(((FragmentActivity) context.getActivity()).getSupportFragmentManager(), "Loading");
            dialog.setMsg(tip);
        }
        FileUploadRequest request = new FileUploadRequest(url, fileParms, requstParms, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {

                if (!StringUtil.isNullOrEmpty(tip)) {
                    if (dialog.getShowsDialog()) {
                        dialog.dismiss();
                    }
                }
                //不做处理，交给应用解析
                listener.onResult(s);



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onError(error);
                if (!StringUtil.isNullOrEmpty(tip)) {
                    if (dialog.getShowsDialog()) {
                        dialog.dismiss();
                    }
                }
            }
        });

        addRequest(request, context);
    }

    /**
     * 返回对象 带进度条
     *
     * @param url      接口
     * @param context  上下文
     * @param clazz    类对象
     * @param params   post需要传的参数
     * @param tip      进度条文字
     * @param listener 回调
     */
    public static <T> void post(final Context context, String url, Object params, @Nullable final String tip, final Class<T> clazz, final ListResponseListener<T> listener) {
        if (!CommonUtils.isNetWorkConnected(context)) {
            ToastUtil.showToast(context, "网络异常");
            return;
        }

        if (!StringUtil.isNullOrEmpty(tip)) {
            dialog = new LoadingFragment();
            dialog.show(((FragmentActivity) context).getSupportFragmentManager(), "Loading");
            dialog.setMsg(tip);
        }
        DefaultJsonRequest jsonObjectRequest = new DefaultJsonRequest(url, params, new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {

                if (!StringUtil.isNullOrEmpty(tip)) {
                    if (dialog.getShowsDialog()) {
                        dialog.dismiss();
                    }
                }
                try {
                    String code = response.getString("Code");
                    if (code.equals("200")) {
                        String content = response.getString("Content");
                        if (!StringUtil.isNullOrEmpty(content)) {
                            List<T> list = JsonUtil.toList(content, clazz);
                            listener.onResult(list);
                        }


                    } else if (code.equals("203")) {

                        ToastUtil.showToast(context, "密码错误");
                    } else if (code.equals("204")) {

                        ToastUtil.showToast(context, "用户名已存在 !");
                    } else if (code.equals("205")) {

                        ToastUtil.showToast(context, "信息已经提交过了 !");
                    } else if (code.equals("202")) {

                        ToastUtil.showToast(context, "保存失败，信息未做修改 !");
                    } else {

                        ToastUtil.showToast(context, "服务器出错了");

                    }
                } catch (JSONException e) {
                    ToastUtil.showToast(context, "服务器出错了");
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onError(error);
                if (!StringUtil.isNullOrEmpty(tip)) {
                    if (dialog.getShowsDialog()) {
                        dialog.dismiss();
                    }
                }
            }
        });

        addRequest(jsonObjectRequest, context);
    }


    /**
     * 返回对象 带进度条
     *
     * @param url      接口
     * @param context  上下文
     * @param params   post需要传的参数
     * @param tip      进度条文字
     * @param listener 回调
     */
    public static <T> void post(final Fragment context, final String url, final Object params, @Nullable final String tip, final Class<T> clazz, final ListResponseListener<T> listener) {

        if (!CommonUtils.isNetWorkConnected(context.getActivity())) {
            ToastUtil.showToast(context.getActivity(), "网络异常");

            return;
        }


        if (!StringUtil.isNullOrEmpty(tip)) {
            dialog = new LoadingFragment();
            dialog.show(((FragmentActivity) context.getActivity()).getSupportFragmentManager(), "Loading");
            dialog.setMsg(tip);
        }
        DefaultJsonRequest jsonObjectRequest = new DefaultJsonRequest(url, params, new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {

                if (!StringUtil.isNullOrEmpty(tip)) {
                    if (dialog.getShowsDialog()) {
                        dialog.dismiss();
                    }
                }
                try {

                    String code = response.getString("Code");
                    if (code.equals("200")) {
                        String content = response.getString("Content");
                        if (!StringUtil.isNullOrEmpty(content)) {

                            List<T> list = JsonUtil.toList(response.getString("Content"), clazz);
                            listener.onResult(list);

                        }


                    } else if (code.equals("203")) {

                        ToastUtil.showToast(context.getActivity(), "密码错误");
                    } else if (code.equals("204")) {

                        ToastUtil.showToast(context.getActivity(), "用户名已存在 !");
                    } else if (code.equals("205")) {

                        ToastUtil.showToast(context.getActivity(), "信息已经提交过了 !");
                    } else if (code.equals("202")) {

                        ToastUtil.showToast(context.getActivity(), "保存失败，信息未做修改 !");
                    } else {

                        ToastUtil.showToast(context.getActivity(), "服务器出错了");

                    }
                } catch (Exception e) {
                    ToastUtil.showToast(context.getActivity(), "服务器出错了");
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onError(error);
                if (!StringUtil.isNullOrEmpty(tip)) {
                    if (dialog.getShowsDialog()) {
                        dialog.dismiss();
                    }
                }
            }
        });

        addRequest(jsonObjectRequest, context);
    }


    public static void addRequest(Request<?> request, Object tag) {
        if (tag != null) {
            request.setTag(tag);
        }
        if (context == null) {
            if (tag instanceof Context) {
                context = (Context) tag;
            } else if (tag instanceof Fragment) {
                context = ((Fragment) tag).getActivity();
            }
        }
        getRequestQueue().add(request);


    }


    /**
     * 当前页面onStop时调用，从队列中取消这个页面的所有请求
     *
     * @param tag
     */
    public static void cancelAll(Object tag) {

        if (requestQueue != null) {
            requestQueue.cancelAll(tag);
        }


    }
}

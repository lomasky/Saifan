package com.httpUtils;

import com.android.volley.VolleyError;

import java.util.List;

/**
 * Created by ma on 2015/8/13.
 */
public interface StringResponseListener  extends ResponseListener {


    /**
     * 错误
     */
    void onError(VolleyError e);

    /**
     * 成功
     */
    public void onResult(String result);

}

package com.utils;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.Holder;
import com.orhanobut.dialogplus.ListHolder;
import com.orhanobut.dialogplus.OnCancelListener;
import com.orhanobut.dialogplus.OnClickListener;
import com.orhanobut.dialogplus.OnDismissListener;
import com.orhanobut.dialogplus.OnItemClickListener;

import org.w3c.dom.Text;

/**
 * Created by ma on 2015/8/31.
 */
public class DialogUtil {

    public static void showCompleteDialog(Context context,Holder holder, int gravity, BaseAdapter adapter,
                                    OnClickListener clickListener, OnItemClickListener itemClickListener,
                                    OnDismissListener dismissListener, OnCancelListener cancelListener,
                                    boolean expanded) {


        final DialogPlus dialog = DialogPlus.newDialog(context)
                .setContentHolder(  holder)
                .setHeader(R.layout.header)
                .setFooter(R.layout.footer)
                .setCancelable(true)
                .setGravity(gravity)
                .setAdapter(adapter)
                .setOnClickListener(clickListener)
                .setOnItemClickListener(itemClickListener)
                .setOnDismissListener(dismissListener)
                .setExpanded(expanded)
                .setContentWidth(ViewGroup.LayoutParams.MATCH_PARENT)
                .setContentHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                .setOnCancelListener(cancelListener)
                        //                .setOutMostMargin(0, 100, 0, 0)
                .create();
        dialog.show();
    }

     public static void showSingle(Context context, final BaseAdapter adapter,     OnItemClickListener listener) {

        View header=View.inflate(context,R.layout.header,null);
            ((TextView) header.findViewById(R.id.title)).setText(R.string.select);
            ((TextView) header.findViewById(R.id.sub_title)).setText(R.string.single);
        final DialogPlus dialog     = DialogPlus.newDialog(context)
                .setContentHolder(new ListHolder())
                .setHeader(header)
                .setCancelable(true)
                .setGravity(Gravity.BOTTOM)
                .setAdapter(adapter)
                .setOnItemClickListener(listener)
                .setOnDismissListener(new OnDismissListener() {
                    @Override
                    public void onDismiss(DialogPlus dialogPlus) {
                        dialogPlus.dismiss();
                    }
                })
                 
                .setExpanded(false)
                .setContentWidth(ViewGroup.LayoutParams.MATCH_PARENT)
                .setContentHeight(ViewGroup.LayoutParams.WRAP_CONTENT)

                .create();
        dialog.show();
    }


    public static void showSingle(Context context, final BaseAdapter adapter,  String titel ,String sub_titel,  OnItemClickListener listener) {

        View header=View.inflate(context,R.layout.header,null);
        if(titel!=null){
            ((TextView) header.findViewById(R.id.title)).setText(titel);

        }
        if (sub_titel!=null){
            ((TextView) header.findViewById(R.id.sub_title)).setText(sub_titel);
        }

        final DialogPlus dialog     = DialogPlus.newDialog(context)
                .setContentHolder(new ListHolder())
                .setHeader(header)
                .setCancelable(true)
                .setGravity(Gravity.BOTTOM)
                .setAdapter(adapter)
                .setOnItemClickListener(listener)
                .setOnDismissListener(new OnDismissListener() {
                    @Override
                    public void onDismiss(DialogPlus dialogPlus) {
                        dialogPlus.dismiss();
                    }
                })

                .setExpanded(false)
                .setContentWidth(ViewGroup.LayoutParams.MATCH_PARENT)
                .setContentHeight(ViewGroup.LayoutParams.WRAP_CONTENT)

                .create();
        dialog.show();
    }




    public static void showOnlyContentDialog(Context context,Holder holder, int gravity, BaseAdapter adapter,
                                       OnItemClickListener itemClickListener, OnDismissListener dismissListener,
                                       OnCancelListener cancelListener, boolean expanded) {
        final DialogPlus dialog = DialogPlus.newDialog(context)
                .setContentHolder(  holder)
                .setGravity(gravity)
                .setAdapter(adapter)
                .setOnItemClickListener(itemClickListener)
                .setOnDismissListener(dismissListener)
                .setOnCancelListener(cancelListener)
                .setExpanded(expanded)
                .setCancelable(true)
                .create();
        dialog.show();
    }
}

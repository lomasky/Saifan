package com.utils;

import com.alibaba.fastjson.JSON;

import java.util.List;

/**
 * json管理
 *
 * @author Administrator
 */
public class JsonUtil {


    public static <T> T object(String json, Class<T> classOfT) {
        return JSON.parseObject(json, classOfT);

    }

    public static String toJson(Object object) {
        return JSON.toJSONString(object);
    }

    public static <T> List<T> toList(String json, Class<T> classOfT) {

        return JSON.parseArray(json, classOfT);
    }

}

package com.utils;

import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 日志工具类
 * @author wangli
 *
 */
public class LogUtil {
	
	private static boolean DEBUG = true;
	private static String tag = "saifan";
	public static void appendLogToFile(String filename, String data) {
		File f = new File(filename);
		if (!f.exists()) {
			try {
				f.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			data += "\r\n";
			FileOutputStream fos = new FileOutputStream(f, true);
			fos.write(data.getBytes());
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	/** 
	 * @Title: setDebugMode
	 * @param debug
	 * @throw
	 * @return void
	 */
	public static void setDebugMode(boolean debug) {
		DEBUG = debug;
	}
	public static void d(String msg) {
		if (DEBUG) {
			Log.d(tag, msg);
		}
	}

	public static void d(String msg, Throwable t) {
		if (DEBUG) {
			Log.d(tag, msg, t);
		}
	}

	public static void e(String msg) {
		if (DEBUG) {
			Log.e(tag, msg);
		}
	}

	public static void e(Object msg) {
		if (DEBUG) {
			Log.e(tag, msg.toString());
		}
	}
	public static void e(String msg, Exception e) {
		if (DEBUG) {
			Log.w(tag, msg, e);
		}
	}

	public static void w(String msg) {
		if (DEBUG) {
			Log.w(tag, msg);
		}
	}

	public static void w(String msg, Exception e) {
		if (DEBUG) {
			Log.w(tag, msg, e);
		}
	}

	public static void i(String msg) {
		if (DEBUG) {
			Log.i(tag, msg);
		}
	}

	public static void v(String msg) {
		if (DEBUG) {
			Log.v(tag, msg);
		}
	}
}

package com.utils;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * @author alessandro.balocco
 */
public class OptionAdapter extends BaseAdapter {

  private LayoutInflater layoutInflater;
  private boolean isGrid=false;
  public List<OptionItem> items;

  public OptionAdapter(Context context, @NonNull List<OptionItem> items, boolean isGrid) {
    layoutInflater = LayoutInflater.from(context);
    this.isGrid = isGrid;
    this.items= items;
  }
  public OptionAdapter(Context context, @NonNull List<OptionItem> items) {
    layoutInflater = LayoutInflater.from(context);
    isGrid=false;
    this.items= items;
  }

  @Override
  public int getCount() {
    return items.size();
  }

  @Override
  public Object getItem(int position) {
    return position;
  }

  @Override
  public long getItemId(int position) {
    return position;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    ViewHolder viewHolder;
    View view = convertView;

    if (view == null) {
      if (isGrid) {
        view = layoutInflater.inflate(R.layout.simple_grid_item, parent, false);
      } else {
        view = layoutInflater.inflate(R.layout.simple_list_item, parent, false);
      }

      viewHolder = new ViewHolder();
      viewHolder.textView = (TextView) view.findViewById(R.id.text_view);
      viewHolder.imageView = (ImageView) view.findViewById(R.id.image_view);
      view.setTag(viewHolder);
    } else {
      viewHolder = (ViewHolder) view.getTag();
    }


    viewHolder.textView.setText(items.get(position).getText());
    viewHolder.imageView.setImageResource(items.get(position).getR());

    return view;
  }

  static class ViewHolder {
    TextView textView;
    ImageView imageView;
  }
}

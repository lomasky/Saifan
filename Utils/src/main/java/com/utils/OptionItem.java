package com.utils;

/**
 * Created by ma on 2015/8/31.
 */
public class OptionItem {
    private  String text;
    private  int r;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getR() {
        return r;
    }

    public void setR(int r) {
        this.r = r;
    }

   private OptionItem(){}
    public OptionItem(String text, int r) {
        this.text = text;
        this.r = r;
    }
}

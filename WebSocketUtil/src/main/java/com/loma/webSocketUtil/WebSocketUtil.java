package com.loma.webSocketUtil;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;

/**
 * Created by ma on 2015/9/11.
 */
public class WebSocketUtil {
    static  MsgHandle msgHandle;
    public static WebSocketClient get(String uri){



        return  new WebSocketClient(URI.create(uri)) {
            @Override
            public void onOpen(ServerHandshake serverHandshake) {

            }

            @Override
            public void onMessage(String s) {

                msgHandle.onMsg(s);


            }

            @Override
            public void onClose(int i, String s, boolean b) {

            }

            @Override
            public void onError(Exception e) {

            }
        };
    }


}

package com.loma.xui;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewStub;

import com.malinskiy.superrecyclerview.SuperRecyclerView;

/**
 * Created by ma on 2015/7/22.
 */
public class ArrayView extends SuperRecyclerView {

    private LinearLayoutManager mLayoutManager;
    private int mProgressId;

    public ArrayView(Context context) {
        super(context);

    }

    public void initViews(Context context) {
        setLayoutManager(mLayoutManager);
        mLayoutManager = new LinearLayoutManager(context);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        setLayoutManager(mLayoutManager);
        setRefreshingColorResources(android.R.color.holo_orange_light, android.R.color.holo_blue_light, android.R.color.holo_green_light, android.R.color.holo_red_light);
        addItemDecoration(new ItemDivider(context));


    }

    public ArrayView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
        initViews(context);


    }

    public ArrayView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initViews(context);

    }

}

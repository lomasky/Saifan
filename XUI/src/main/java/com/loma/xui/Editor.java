package com.loma.xui;

import android.content.Context;
import android.util.AttributeSet;

import com.rengwuxian.materialedittext.MaterialEditText;

/**
 * Created by ma on 2015/8/12.
 */
public class Editor extends MaterialEditText {
    public Editor(Context context) {
        super(context);
    }

    public Editor(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public Editor(Context context, AttributeSet attrs, int style) {
        super(context, attrs, style);
    }

}

package com.saifan;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import com.android.volley.VolleyError;
import com.httpUtils.Http;
import com.httpUtils.ListResponseListener;
import com.saifan.data.bean.Room;
import com.saifan.data.bean.UserInfo;
import com.saifan.databinding.ActivityLoginBinding;

import java.util.List;

public class LoginActivity extends BaseActivity {
    ActivityLoginBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
    }

    public void login(View view) {


        UserInfo userInfo = new UserInfo();
        userInfo.setUserName(binding.userName.getText().toString().trim());
        userInfo.setPassword(binding.password.getText().toString().trim());
        Http.post(this, API.UserService.LOGIN, userInfo, "Login", Room.class, new ListResponseListener<Room>() {
            @Override
            public void onError(VolleyError e) {

            }

            @Override
            public void onResult(List<Room> result) {


            }
        });



    }






}

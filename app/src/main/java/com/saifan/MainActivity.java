package com.saifan;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;


import com.android.volley.VolleyError;
import com.httpUtils.Http;
import com.httpUtils.URLConfig;
import com.loma.xui.MyAdapter;
import com.loma.xui.ArrayView;
import com.malinskiy.superrecyclerview.OnMoreListener;
import com.saifan.data.bean.Room;
import com.saifan.data.bean.UserInfo;
import com.saifan.databinding.ActivityMainBinding;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {
    private Handler mHandler;
    List<Room> list = new ArrayList<>();
    private ActivityMainBinding binding;



    private MyAdapter mAdapter;
    private DrawerLayout mDrawerLayout;

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_menu);
        ab.setDisplayHomeAsUpEnabled(true);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (navigationView != null) {
            setupDrawerContent(navigationView);
        }
        mHandler = new Handler(Looper.getMainLooper());

        mAdapter = new MyAdapter(list, com.saifan.BR.room, R.layout.list_item);



        binding.recyclerView.setAdapter(mAdapter);
        binding.recyclerView.getEmptyView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.recyclerView.getEmptyView().setVisibility(View.GONE);
                binding.recyclerView.getProgressView().setVisibility(View.VISIBLE);
                mHandler.postDelayed(new Runnable() {
                    public void run() {

                        list.addAll(MyApp.myApp.getDaoSession().loadAll(Room.class));
                        mAdapter.mDatas = list;
                        mAdapter.notifyDataSetChanged();
                        binding.recyclerView.getProgressView().setVisibility(View.GONE);
                    }
                }, 2000);

            }
        });

        binding.recyclerView.setRefreshListener(this);
        binding.recyclerView.setupMoreListener(new OnMoreListener() {
            @Override
            public void onMoreAsked(int numberOfItems, int numberBeforeMore, int currentItemPos) {
                // Fetch more from Api or DB
                mHandler.postDelayed(new Runnable() {
                    public void run() {
                        binding.recyclerView.getMoreProgressView().setVisibility(View.GONE);
//                        list.addAll(MyApp.myApp.getDaoSession().loadAll(Room.class));
//                        mAdapter.mDatas = list;
//                        mAdapter.notifyDataSetChanged();
                    }
                }, 2000);

            }
        }, 10);


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (!mDrawerLayout.isDrawerVisible(GravityCompat.START)){
                    mDrawerLayout.openDrawer(GravityCompat.START);
                }else {
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                }

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        menuItem.setChecked(true);
                        mDrawerLayout.closeDrawers();
                        return true;
                    }
                });
    }


    @Override
    public void onRefresh() {
        Toast.makeText(this, "Refresh", Toast.LENGTH_LONG).show();


        mHandler.postDelayed(new Runnable() {
            public void run() {
                binding.recyclerView.getSwipeToRefresh().setRefreshing(false);
            }
        }, 2000);
    }

    /**
     *
     * */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }



}

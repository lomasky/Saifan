package com.saifan;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;


import com.saifan.data.dao.DaoMaster;
import com.saifan.data.dao.DaoSession;
import com.utils.CommonUtils;

import java.io.File;

/**
 * Created by ma on 2015/7/20.
 */
public class MyApp extends Application {

    public static String AppFile;
    public static MyApp myApp;

    @Override
    public void onCreate() {
        super.onCreate();
        myApp = this;

        initAppFile();
        setupDatabase();

    }

    private void initAppFile() {
        AppFile = CommonUtils.getSDCardPath() + "/saifan/";
        File file = new File(AppFile);
        // 检测路径是否存在
        if (!file.exists()) {
            file.mkdirs();// 创建该路径
        }
    }

    public DaoSession daoSession;


    private void setupDatabase() {
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, AppFile + "app.db", null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        daoSession = daoMaster.newSession();

    }

    public DaoSession getDaoSession() {
        return daoSession;
    }
}


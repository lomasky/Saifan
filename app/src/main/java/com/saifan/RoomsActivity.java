package com.saifan;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.loma.xui.MyAdapter;
import com.saifan.data.bean.Room;
import com.saifan.databinding.ActivityRoomsBinding;

import java.util.List;

/**
 * @version V1.0.0
 * @author: lomasky
 * @date: 2015-8-17 16:42:01
 * @descrption
 */
public class RoomsActivity extends BaseActivity {

    ActivityRoomsBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_rooms);

       List<Room> rooms= MyApp.myApp.getDaoSession().loadAll(Room.class);
        if (rooms!=null){
            MyAdapter adapter = new MyAdapter(rooms, com.saifan.BR.room,R.layout.rooms);
            binding.arrayView.setAdapter(adapter);
        }



    }


}

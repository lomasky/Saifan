package com.saifan.data.bean;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.saifan.BR;

/**
 * Created by ma on 2015/7/6.
 */
public class Room extends BaseObservable {



    /**
     * WYKHDA_ZJHM : 1
     * WYKHDA_KHMC : 马勇刚
     * WYKHDA_ZJ : 00100100000000000002
     * WYKHDA_WXH : 5522
     * WYFJDA_FJMC : 01-01B
     * WYKHDA_LC : 你烦然
     * WYKHDA_SJ : 5
     * WYFJDA_ZJ : 00100100000000000002
     * WYFJDA_HX :
     * XMBS : 100100
     * XMDA_XMMC : 新天地大厦
     * WYKHDA_ZJMC : null
     * WYKHDA_TXLJ : /Upload/HeadPhoto/2015814/f658a4fa-2e64-48b2-81b6-888787d06f47.jpg
     * WYFJDA_CX : null
     * WYKHDA_KHYHM : 5
     * FKGX_ZJ : 00100100000000000002
     * WYKHDA_XB : null
     * WYFJDA_JZMC : 100
     */
//1
    @Bindable
    private String WYKHDA_ZJHM;
    //马勇刚
    @Bindable
    private String WYKHDA_KHMC;
    //00100100000000000002
    @Bindable
    private String WYKHDA_ZJ;
    //5522
    @Bindable
    private String WYKHDA_WXH;
    //01-01B
    @Bindable
    private String WYFJDA_FJMC;
    //你烦然
    @Bindable
    private String WYKHDA_LC;
    //5
    @Bindable
    private String WYKHDA_SJ;
    //00100100000000000002
    @Bindable
    private String WYFJDA_ZJ;
    //
    @Bindable
    private String WYFJDA_HX;
    //100100
    @Bindable
    private int XMBS;
    //新天地大厦
    @Bindable
    private String XMDA_XMMC;
    //null
    @Bindable
    private String WYKHDA_ZJMC;
    ///Upload/HeadPhoto/2015814/f658a4fa-2e64-48b2-81b6-888787d06f47.jpg
    @Bindable
    private String WYKHDA_TXLJ;
    //null
    @Bindable
    private String WYFJDA_CX;
    //5
    @Bindable
    private String WYKHDA_KHYHM;
    //00100100000000000002
    @Bindable
    private String FKGX_ZJ;
    //null
    @Bindable
    private String WYKHDA_XB;
    //100
    @Bindable
    private int WYFJDA_JZMC;

    public Room(String WYKHDA_ZJHM, String WYKHDA_KHMC, String WYKHDA_ZJ, String WYKHDA_WXH, String WYFJDA_FJMC, String WYKHDA_LC, String WYKHDA_SJ, String WYFJDA_ZJ, String WYFJDA_HX, int XMBS, String XMDA_XMMC, String WYKHDA_ZJMC, String WYKHDA_TXLJ, String WYFJDA_CX, String WYKHDA_KHYHM, String FKGX_ZJ, String WYKHDA_XB, int WYFJDA_JZMC) {
        this.WYKHDA_ZJHM = WYKHDA_ZJHM;
        this.WYKHDA_KHMC = WYKHDA_KHMC;
        this.WYKHDA_ZJ = WYKHDA_ZJ;
        this.WYKHDA_WXH = WYKHDA_WXH;
        this.WYFJDA_FJMC = WYFJDA_FJMC;
        this.WYKHDA_LC = WYKHDA_LC;
        this.WYKHDA_SJ = WYKHDA_SJ;
        this.WYFJDA_ZJ = WYFJDA_ZJ;
        this.WYFJDA_HX = WYFJDA_HX;
        this.XMBS = XMBS;
        this.XMDA_XMMC = XMDA_XMMC;
        this.WYKHDA_ZJMC = WYKHDA_ZJMC;
        this.WYKHDA_TXLJ = WYKHDA_TXLJ;
        this.WYFJDA_CX = WYFJDA_CX;
        this.WYKHDA_KHYHM = WYKHDA_KHYHM;
        this.FKGX_ZJ = FKGX_ZJ;
        this.WYKHDA_XB = WYKHDA_XB;
        this.WYFJDA_JZMC = WYFJDA_JZMC;
    }

    public void setWYKHDA_ZJHM(String WYKHDA_ZJHM) {
        this.WYKHDA_ZJHM = WYKHDA_ZJHM;
        notifyPropertyChanged(BR.WYKHDA_ZJHM);
    }

    public void setWYKHDA_KHMC(String WYKHDA_KHMC) {
        this.WYKHDA_KHMC = WYKHDA_KHMC;
        notifyPropertyChanged(BR.WYKHDA_KHMC);
    }

    public void setWYKHDA_ZJ(String WYKHDA_ZJ) {
        this.WYKHDA_ZJ = WYKHDA_ZJ;
        notifyPropertyChanged(BR.WYKHDA_ZJ);
    }

    public void setWYKHDA_WXH(String WYKHDA_WXH) {
        this.WYKHDA_WXH = WYKHDA_WXH;
        notifyPropertyChanged(BR.WYKHDA_WXH);
    }

    public void setWYFJDA_FJMC(String WYFJDA_FJMC) {
        this.WYFJDA_FJMC = WYFJDA_FJMC;
        notifyPropertyChanged(BR.WYFJDA_FJMC);
    }

    public void setWYKHDA_LC(String WYKHDA_LC) {
        this.WYKHDA_LC = WYKHDA_LC;
        notifyPropertyChanged(BR.WYKHDA_LC);
    }

    public void setWYKHDA_SJ(String WYKHDA_SJ) {
        this.WYKHDA_SJ = WYKHDA_SJ;
        notifyPropertyChanged(BR.WYKHDA_SJ);
    }

    public void setWYFJDA_ZJ(String WYFJDA_ZJ) {
        this.WYFJDA_ZJ = WYFJDA_ZJ;
        notifyPropertyChanged(BR.WYFJDA_ZJ);
    }

    public void setWYFJDA_HX(String WYFJDA_HX) {
        this.WYFJDA_HX = WYFJDA_HX;
        notifyPropertyChanged(BR.WYFJDA_HX);
    }

    public void setXMBS(int XMBS) {
        this.XMBS = XMBS;
        notifyPropertyChanged(BR.XMBS);
    }

    public void setXMDA_XMMC(String XMDA_XMMC) {
        this.XMDA_XMMC = XMDA_XMMC;
        notifyPropertyChanged(BR.XMDA_XMMC);
    }

    public void setWYKHDA_ZJMC(String WYKHDA_ZJMC) {
        this.WYKHDA_ZJMC = WYKHDA_ZJMC;
        notifyPropertyChanged(BR.WYKHDA_ZJMC);
    }

    public void setWYKHDA_TXLJ(String WYKHDA_TXLJ) {
        this.WYKHDA_TXLJ = WYKHDA_TXLJ;
        notifyPropertyChanged(BR.WYKHDA_TXLJ);
    }

    public void setWYFJDA_CX(String WYFJDA_CX) {
        this.WYFJDA_CX = WYFJDA_CX;
        notifyPropertyChanged(BR.WYFJDA_CX);
    }

    public void setWYKHDA_KHYHM(String WYKHDA_KHYHM) {
        this.WYKHDA_KHYHM = WYKHDA_KHYHM;
        notifyPropertyChanged(BR.WYKHDA_KHYHM);
    }

    public void setFKGX_ZJ(String FKGX_ZJ) {
        this.FKGX_ZJ = FKGX_ZJ;
        notifyPropertyChanged(BR.FKGX_ZJ);
    }

    public void setWYKHDA_XB(String WYKHDA_XB) {
        this.WYKHDA_XB = WYKHDA_XB;
        notifyPropertyChanged(BR.WYKHDA_XB);
    }

    public void setWYFJDA_JZMC(int WYFJDA_JZMC) {
        this.WYFJDA_JZMC = WYFJDA_JZMC;
        notifyPropertyChanged(BR.WYFJDA_JZMC);
    }

    public String getWYKHDA_ZJHM() {
        return WYKHDA_ZJHM;
    }

    public String getWYKHDA_KHMC() {
        return WYKHDA_KHMC;
    }

    public String getWYKHDA_ZJ() {
        return WYKHDA_ZJ;
    }

    public String getWYKHDA_WXH() {
        return WYKHDA_WXH;
    }

    public String getWYFJDA_FJMC() {
        return WYFJDA_FJMC;
    }

    public String getWYKHDA_LC() {
        return WYKHDA_LC;
    }

    public String getWYKHDA_SJ() {
        return WYKHDA_SJ;
    }

    public String getWYFJDA_ZJ() {
        return WYFJDA_ZJ;
    }

    public String getWYFJDA_HX() {
        return WYFJDA_HX;
    }

    public int getXMBS() {
        return XMBS;
    }

    public String getXMDA_XMMC() {
        return XMDA_XMMC;
    }

    public String getWYKHDA_ZJMC() {
        return WYKHDA_ZJMC;
    }

    public String getWYKHDA_TXLJ() {
        return WYKHDA_TXLJ;
    }

    public String getWYFJDA_CX() {
        return WYFJDA_CX;
    }

    public String getWYKHDA_KHYHM() {
        return WYKHDA_KHYHM;
    }

    public String getFKGX_ZJ() {
        return FKGX_ZJ;
    }

    public String getWYKHDA_XB() {
        return WYKHDA_XB;
    }

    public int getWYFJDA_JZMC() {
        return WYFJDA_JZMC;
    }

    public Room() {
    }

}

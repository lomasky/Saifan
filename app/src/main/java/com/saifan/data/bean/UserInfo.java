package com.saifan.data.bean;


import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.saifan.BR;

/**
 * Created by ma on 2015/7/1.
 */
public class UserInfo extends BaseObservable {

    @Bindable
    private String UserName;
    @Bindable
    private String Password;
    @Bindable
    private String Photo;


    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
        notifyPropertyChanged(BR.Id);
    }

    @Bindable
    private int Id;
    public UserInfo() {
    }

    public UserInfo(String userName, String password, String photo, int id) {
        UserName = userName;
        Password = password;
        Photo = photo;
        Id = id;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String Password) {
        this.Password = Password;
        notifyPropertyChanged(BR.Password);

    }

    public String getPhoto() {
        return Photo;
    }

    public void setPhoto(String photo) {
        this.Photo = photo;
    }

    public String getUserName() {
        return UserName;
    }


    public void setUserName(String UserName) {
        this.UserName = UserName;
        notifyPropertyChanged(BR.UserName);


    }
}